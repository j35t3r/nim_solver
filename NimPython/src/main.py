#!/usr/bin/python3

import sys

# For the Xor connection, we will calc all numbers with 9 bits length, because
# 512 = 2^9, and 400 is the absolut max, what a nim can be (for this example)

def deleteAllDuplicates(list_old):
    list_old.sort()
    list_new = []

    elem = list_old[0]
    list_new.append(elem)

    for i in range(1, len(list_old)):
        if elem != list_old[i]:
            elem = list_old[i]
            list_new.append(elem)

    return list_new

def calcAllCombinationsFor2(size, possible_stacks):
    for i in range(1, int(size/2)):
        if size - i >= i:
            possible_stacks.append([size - i, i])
        else:
            break

def calcAllCombinationsFor3(size, possible_stacks):
    for i2 in range(1, int(size/3)):
        for i1 in range(i2, int(size/2)):
            if size - i1 - i2 >= i1:
                possible_stacks.append([size - i1 - i2, i1, i2])
            else:
                break

def calcAllCombinationsFor4(size, possible_stacks):
    for i3 in range(1, int(size/4)):
        for i2 in range(i3, int(size/3)):
            for i1 in range(i2, int(size/2)):
                if size - i1 - i2 - i3 >= i1:
                    possible_stacks.append([size - i1 - i2 - i3, i1, i2, i3])
                else:
                    break


# def recursiveCalcNim(stacks, rules, nim_dict, nims_of_nim_dict):
def getAllPossibleStacks(height, m, h):
    possible_stacks = []

    if (height - m) / h >= 1:
        height -= m
        start_stack = [1 for _ in range(0, h)]
        start_stack[0] = height - h + 1

        if len(start_stack) > 1:
            # Do all combinations
            # e.g. [4,1,1,1] is given, we can see, that the stack has
            # a sie of 7, because 1+1+1+4 = 7
            # do all combiations would look like:
            # [4,1,1,1]
            # [3,2,1,1]
            # [2,2,2,1]

            # First, do all primitive stacks
            # e.g.: [9,1,1,1]
            #       [8,2,1,1]
            #       [7,3,1,1]
            #       [6,4,1,1]
            #       [5,5,1,1]
            stacks = [start_stack]
            while stacks[-1][0] - 1 >= stacks[-1][1] + 1:
                stacks.append(list([stacks[-1][0] - 1, stacks[-1][1] + 1] + stacks[-1][2:]))

            for s in stacks: possible_stacks.append(s)

            for depth in range(2, h):
                is_changed = True

                while is_changed:   # check, if in the stacks will come something new
                    new_stacks = []
                    is_changed = False

                    for s in stacks:
                        if s[0] - depth >= s[1] + 1:
                            is_changed = True
                            temp = list(s)   # make a copy
                            temp[0] -= depth
                            for i in range(1, depth + 1): temp[i] += 1
                            new_stacks.append(temp)

                    for s in new_stacks: possible_stacks.append(s)
                    stacks = new_stacks

                stacks = possible_stacks
            pass

    return possible_stacks

def calculateNim(rules):
    # TODO: MAYBE NOT NEEDED!?
    # # First get all combinations as raw information (as stack size)
    # possible_all_stacks = [[0]]

    # The final nim number of the stacksize
    # list defition as: stacksize -> nim number
    nim_list = [0]

    # TODO: MAYBE ALSO NOT NEEDED!?!?
    # All possible nims of each game
    # list defition as: stacksize -> all possible nims
    nims_of_nim_list = [[0]]

    print("calculate rules")
    print('Count rules:',len(rules))
    # min = rules[0]
    # max = rules[1]
    # h2 = rules[2]
    # h3 = rules[2]

    print("0: 0")

    # Now calc all nims for 1 to up 400 stack size
    for height in range(1, 101): # 401):
        # nims_of_nim = []
        # print("height = "+str(height))
        possible_stacks = []

        for rule in rules:
            m1 = rule[0]
            m2 = rule[1]
            for m in range(m1, m2 + 1):
                if height >= m:
                    for h in rule[2:]:
                        if h == 0:
                            if height - m == 0:
                                possible_stacks.append([0])  # TODO: is it correct?!?!
                        elif h == 1:
                            possible_stacks.append([height - m])
                        elif h == 2:
                            calcAllCombinationsFor2(height - m, possible_stacks)
                        elif h == 3:
                            calcAllCombinationsFor3(height - m, possible_stacks)
                        elif h == 4:
                            calcAllCombinationsFor4(height - m, possible_stacks)

        # Get all Xor calcs from stacks
        nim = []

        deleteAllDuplicates(possible_stacks)
        for ps in possible_stacks:
            temp_nim = nim_list[ps[0]]
            for s in ps[1:]:
                temp_nim ^= nim_list[s]
            nim.append(temp_nim)

        # nim = []
        # for ps in possible_stacks:
        #     nim.append(getXorNimNumber(nim_list, ps))

        # Calc the smallest not reached nim number
        if len(nim) > 0:
            nim = deleteAllDuplicates(nim)

            for i in range(0, 401):
                if i >= len(nim):
                    nim_list.append(i)
                    break

                if i != nim[i]:
                    nim_list.append(i)
                    break
        else:
            nim_list.append(0)

        print(str(height)+": "+str(nim_list[-1]))
        # print("all nimbs without duplications: "+str(nimbs))

    print("all calculated nim's:")
    for height, nim in zip(range(0, len(nim_list)), nim_list):
        print(str(height)+": "+str(nim))

    return nim_list

def cleanNim(rules):
    print("calculate NIM")
    for x in rules[0]:
        if not isinstance(x, int) and x != "INF":
            rules[0].pop(0)
            break

    # Convert all numbers to int
    rules = [[int(i) if i != "INF" else 400 for i in rule] for rule in rules]
    print("Rules = "+str(rules))

    # Get the nim's of all games up to 400 per stack
    nims = calculateNim(rules)

    print(rules)

    return rules, nims

def playNimGame(rules, nims, stacks):
    # First calc who should begin
    # Nim 0 -> play should begin
    # Nim > 0 -> Computer should begin

    # print("rules = "+str(rules))
    # print("nims = "+str(nims))
    # print("stacks = "+str(stacks))

    temp = nims[stacks[0]]
    for s in stacks[1:]:
        temp ^= nims[s]

    nim_game = temp

    is_computers_turn = True  # Computer begins

    if nim_game == 0:    # Player begins
        is_computers_turn = False

    print("nim of this game is = "+str(nim_game))

    # Calc all nims of stack with xor nim of the game

    nim_stacks = [nims[s] for s in stacks]
    nim_next_stacks = [s ^ nim_game for s in nim_stacks]



def parseFile(file):
    print("parse file\n")
    print(file)
    # array = []
    # for x in file:
    #     a = x.split(" ")
    #     array.append(a)
    array = [x.split(" ") for x in file]

    # print(array)
    return cleanNim(array)

def readInFile(file):
    print('read in file')
    tmp = []
    with open(file, "r") as game: #"with" is better for errors
        for line in game:
            line = line.rstrip('\n')
            tmp.append(line)
    return tmp

if __name__ == '__main__':
    print('main')
    # try:
    if len(sys.argv) != 2:
        print("Error: Wrong argument count!")
        print("Usage: <./main.py> <game.txt>")
        sys.exit(-1)
    rules, nims = parseFile(readInFile(sys.argv[1]))

    inpstr = input("Please enter the stack sizese (seperated with spaces):\n")
    stack_sizes = [int(i) for i in inpstr.split(" ")]
    print("stack_sizes = "+str(stack_sizes))
    playNimGame(rules, nims, stack_sizes)
    # all_stack_nims_xor_combs = calcAllNimsXorCombinations(nims)

    # print("all stack nims xor combs = "+str(all_stack_nims_xor_combs))
    for n, i in zip(nims, range(0, 12)):
        print(str(i)+": "+str(n))
    # except:
    #     print('error happend!\n')
