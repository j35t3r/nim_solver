import java.util.Arrays;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;

class Main {
  public int returnVal(int i) {
    return i * 2 + 1;
  }

  // copied from stackoverflow
  public static boolean isNumeric(String str)
  {
    try
    {
      double d = Double.parseDouble(str);
    }
    catch(NumberFormatException nfe)
    {
      return false;
    }
    return true;
  }

  public static ArrayList<List<String>> readFile(String[] args) {

    try (Stream<String> stream = Files.lines(Paths.get(args[0]))) {

      ArrayList<List<String>> beer = new ArrayList<List<String>>();
      String[] stringArray = stream.toArray(size ->new String[size]);

      for (int i = 0; i < stringArray.length; i++) {
        ArrayList<String> mylist = new ArrayList<String>(Arrays.asList(stringArray[i].split(" ")));
        beer.add(mylist);
      }

      System.out.println(beer);
      for (int i = 0; i < beer.size(); i++) {
        Iterator<String> it = beer.get(i).iterator();
        while(it.hasNext())
        {
          String tmp = it.next();
          if(isNumeric(tmp) == false && !tmp.equals("INF"))
          {
            System.out.println("Is not a Number");
            it.remove();
          }
        }
      }
      return beer;
      } catch (IOException e) {
        e.printStackTrace();
      }
      return new ArrayList<List<String>>();
  }

//  # For the Xor connection, we will calc all numbers with 9 bits length, because
//  # 512 = 2^9, and 400 is the absolut max, what a nim can be (for this example)
  public static ArrayList<List<Integer>> deleteAllDublicates(ArrayList<List<Integer>> list_old) {
    Arrays.sort(list_old.toArray());
    List<Integer> elem = list_old.get(0);
    ArrayList<List<Integer>> list_new = new ArrayList<List<Integer>>();
    list_new.add(elem);

    for (int i = 1; i < list_old.size(); i++) {
      if(!elem.equals(list_old.get(i))) {
        elem = list_old.get(i);
        list_new.add(elem);
      }
    }
    return list_new;
  }

  public static void calcAllCombinationsFor2(int size, ArrayList<List<Integer>> possible_stacks) {
    int val = (int)(size * 0.5);
    for (int i = 1; i < val; i++) {
      if(size - i >= i)
      {
          possible_stacks.add(Arrays.asList(new Integer[]{size - i}));
          possible_stacks.add(Arrays.asList(new Integer[]{i}));
      }
      else
        break;
    }
  }

  public static void calcAllCombinationsFor3(int size, ArrayList<List<Integer>> possible_stacks) {
    int val2 = (int)(size * 0.5);
    int val1 = (int)(size * 0.333333333333333);
    for (int i2 = 1; i2 < val1; i2++) {
      for (int i1 = i2; i1 < val2; i1++) {
      if( size - i1 - i2 >= i1)
      {
        possible_stacks.add(Arrays.asList(new Integer[]{size - i1 - i2}));
        possible_stacks.add(Arrays.asList(new Integer[]{i1}));
        possible_stacks.add(Arrays.asList(new Integer[]{i2}));
      }
      else
        break;
      }
    }
  }

  public static void calcAllCombinationsFor4(int size,  ArrayList<List<Integer>> possible_stacks) {
    int val1 = (int) (size * 0.5);
    int val2 = (int) (size * 0.333333333333333);
    int val3 = (int) (size * 0.25);

    for (int i3 = 1; i3 < val3; i3++) {
      for (int i2 = i3; i2 < val2; i2++) {
        for (int i1 = i2; i1 < val1; i1++) {
          if (size - i1 - i2 - i3 >= i1) {
            possible_stacks.add(Arrays.asList(new Integer[]{size - i1 - i2 - i3}));
            possible_stacks.add(Arrays.asList(new Integer[]{i1}));
            possible_stacks.add(Arrays.asList(new Integer[]{i2}));
            possible_stacks.add(Arrays.asList(new Integer[]{i3}));
          }
          else
            break;
        }
      }
    }
  }

  public static ArrayList<Integer> calculateNIM(ArrayList<List<String>> rules) {
    ArrayList<Integer> nim_list = new ArrayList<Integer>();
    // calc all NIMs from 1 up to 400
    for (int height = 1; height <= 400; height++) {
      ArrayList<List<Integer>> possible_stacks = new ArrayList<List<Integer>>();

      for (List<String> rule : rules) {

        int m = Integer.parseInt(rule.get(0));
        final int m2 = Integer.parseInt(rule.get(0));
        for (; m < m2; m++) {

          if (height >= m)
            for (String hs : rule.subList(2, rule.size())) {
              int h = Integer.parseInt(hs);
              switch (h) {
                case 0:
                  if (height - m == 0)
                    possible_stacks.add(null);
                case 1:
                  possible_stacks.add(Arrays.asList(new Integer[]{height - m}));
                  break;
                case 2:
                  calcAllCombinationsFor2(height - m, possible_stacks);
                  break;
                case 3:
                  calcAllCombinationsFor3(height - m, possible_stacks);
                  break;
                case 4:
                  calcAllCombinationsFor4(height - m, possible_stacks);
                  break;
                default:
                  System.out.println("No case available");
                  break;
              }
            }

          ArrayList<List<Integer>> nim = new ArrayList<List<Integer>>();
          ArrayList<List<Integer>> possible_stack = deleteAllDublicates(possible_stacks);
          for (int ps = 0; ps < possible_stack.size(); ps++) {
            int tmp = nim_list.get(possible_stack.get(ps).get(0));
            for (int s = 1; s < possible_stack.get(ps).size(); s++) {
              tmp = tmp ^ nim_list.get(possible_stack.get(ps).get(s));
            }
            nim.get(ps).add(tmp);
          }
          // Calc the smallest not reached nim number
          if(nim.size() == 0)
          {
            nim_list.add(0);
          }
          else
          {
            nim = deleteAllDublicates(nim);
            for (int i = 0; i <= 400; i++) {
              if(i >= nim.size() /*|| i != nim.get(i)*/) //fixme
              {
                nim_list.add(i);
                System.out.println(nim_list.size());
                break;
              }
            }
          }
        }
//             System.out.println(height + ": " + nim_list.get(height));
      }
    }
    return nim_list;
  }

    public static void main(String[] args) {
    System.out.println("Main");
    if(args.length != 1)
    {
      System.out.println("Error: Wrong argument count!");
      System.out.println("Usage: <./main.py> <game.txt>");
      return;
    }
    //read file into stream,
    ArrayList<List<String>> parsed = readFile(args);
    calculateNIM(parsed);
    System.out.println(parsed);
    }
}
